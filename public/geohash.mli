
(** Geohash math to encode and decode base32 character hashes. *)

(** a WGS84 coordinate pair *)
type wgs84_lat_lon = (float * float)
type delta = (float * float)

(** encode a WGS84 (lat,lon) coordinate pair into Geohash of given
    hash character count. *)
val encode : int -> wgs84_lat_lon -> (string, int) result

(** decode a geohash base32 string to a WGS84 coordinate region center,delta. *)
val decode : string -> ((wgs84_lat_lon * delta), char) result

(**/**) (* hide Whitebox module *)
module Whitebox : module type of Whitebox
(**/**)
