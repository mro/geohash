/* MIT License https://github.com/joliss/js-string-escape/blob/master/index.js */
function jsStringEscape(string) {
  return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (character) {
    // Escape all characters not included in SingleStringCharacters and
    // DoubleStringCharacters on
    // http://www.ecma-international.org/ecma-262/5.1/#sec-7.8.4
    switch (character) {
      case '"':
      case "'":
      case '\\':
        return '\\' + character
      // Four possible LineTerminator characters need to be escaped:
      case '\n':
        return '\\n'
      case '\r':
        return '\\r'
      case '\u2028':
        return '\\u2028'
      case '\u2029':
        return '\\u2029'
    }
  })
}

document.onreadystatechange = function () {
  var a = document.getElementById("bookmarklet");
  a.href = a.href.replace("}('../'));", "}('"+jsStringEscape(window.location)+"/../'));");

  // load iframe content once it's size is set - mitigate FOUC https://stackoverflow.com/a/57675785/349514
  var f = document.getElementById('ifrm_map');
  f.src = f.dataset.src;
}
