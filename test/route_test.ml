(*
 * route_test.ml
 *
 * Created by Marcus Rohrmoser on 16.05.20.
 * Copyright © 2020-2021 Marcus Rohrmoser mobile Software http://mro.name/~me. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Lib.Route

let test_qs () =
  assert (
    match coord_from_qs "q=1.2,3.4" with Ok (1.2, 3.4) -> true | _ -> false);
  assert (
    match coord_from_qs "q=geo%3A47.5440%2C15.4396%3Fz%3D12" with
    | Ok (47.5440, 15.4396) -> true
    | _ -> false);
  match coord_from_qs "q=1.u2 , ; 3.4" with
  | Error (`NoMatch (_, original)) -> assert (original != "")
  | _ -> assert false

let test_deg () =
  (*
  let la, lo =
    "q=47.427%C2%B0,+13.059%C2%B0" |> coord_from_qs |> Result.get_ok
  in
  Printf.printf "test_deg %f, %f" la lo;
  *)
  assert (Ok (47.427, 13.059) = ("q=47.427%C2%B0,+13.059%C2%B0" |> coord_from_qs));
  (* assert (Ok (47.427, 13.059) = ("q=47.427°, 13.059°" |> coord_from_qs)); *)
  (* assert (
     Ok (47.5440, 15.4396) = ("q=47° 25′ 37″ N, 13° 3′ 32″ O" |> coord_from_qs)); *)
  let la, lo =
    "q=47%C2%B0+25%E2%80%B2+37%E2%80%B3+N,+13%C2%B0+3%E2%80%B2+32%E2%80%B3+O"
    |> coord_from_qs |> Result.get_ok
  in
  (* Printf.printf "%f,%f\n" la lo; *)
  assert (47.427 -. la < 1e-1);
  assert (13.059 -. lo < 1e-1);
  let la', lo' =
    "q=47%C2%B0+25%E2%80%B2+37.2%E2%80%B3+N,+13%C2%B0+3%E2%80%B2+32.4%E2%80%B3+E"
    |> coord_from_qs |> Result.get_ok
  in
  (* Printf.printf "%f,%f\n" la' lo'; *)
  assert (47.427 -. la' < 1e-1);
  assert (13.059 -. lo' < 1e-1);
  assert true

let test_deg_min_sec () =
  let open Tyre in
  let dms =
    let ws = pcre "( |\t|\\+|%20)*" in
    conv
      (fun ((d, (m, s)), he) ->
         let si = match he with "S" | "W" -> -1. | _ -> 1.
         and d' = d
         and m' = m
         and s' = s in
         si *. (d' +. ((m' +. (s' /. 60.)) /. 60.)))
      (fun v ->
         let si = if v < 0. then -1. else 1. in
         let d' = v *. si in
         let d = d' |> floor in
         let dr = d' -. d in
         let m' = dr *. 60. in
         let m = m' |> floor in
         let mr' = m' -. m in
         let se = mr' *. 60. in
         ((d, (m, se)), if si < 0. then "S" else "N"))
      (float <* pcre "°" <* ws
       <&> (float <* pcre "′" <* ws <&> (float <* pcre "″" <* ws))
       <&> pcre "[NSEW]")
    |> compile
  in
  let x = "47° 25′ 37″ N" |> exec dms |> Result.get_ok in
  (* Printf.printf "%f" x; *)
  assert (47.426944 -. x < 1.e-16);
  let x' = "47° 25′ 37″ E" |> exec dms |> Result.get_ok in
  (* Printf.printf "%f" x'; *)
  assert (47.426944 -. x' < 1.e-16)

let test_2023_07 () =
  let la, lo =
    "q=53%C2%B0+01%27+48%22+N%2C+13%C2%B0+18%27+27%22+E"
    |> coord_from_qs |> Result.get_ok
  in
  la |> Assert2.equals_float "route test_2023_07" 1e-9 47.427;
  lo |> Assert2.equals_float "route test_2023_07" 1e-9 13.059;
  assert true

let () =
  test_qs ();
  test_deg ();
  test_deg_min_sec ();
  test_2023_07 ();
  assert true
